package com.pokerstars.omaha.actors;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.pokerstars.omaha.data.CardCombinationsInput;
import com.pokerstars.omaha.data.HandResult;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.Tuple2;
import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Either;


public class ResultProcessor extends AbstractBehavior<ResultProcessor.HandleResult> {

    private final String fileOutputName;
    private CardCombinationsInput handsAndBoard;
    private Map<Integer, Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>>> results = HashMap.empty();

    public ResultProcessor(ActorContext<HandleResult> context, CardCombinationsInput handsAndBoard, String fileOutputName) {
        super(context);
        this.handsAndBoard = handsAndBoard;
        this.fileOutputName = fileOutputName;
    }

    public static Behavior<HandleResult> create(CardCombinationsInput handsAndBoard, String fileOutputName) {
        return Behaviors.setup(context -> new ResultProcessor(context, handsAndBoard, fileOutputName));
    }

    @Override
    public Receive<HandleResult> createReceive() {
        return newReceiveBuilder().onMessage(HandleResult.class, this::onHandleResult).build();
    }

    private Behavior<HandleResult> onHandleResult(HandleResult command) {
        if (!results.containsKey(command.processId)) {
            results = results.put(command.processId, command.result);
        } else {
            final HandResult handResult = CardProcessorUtils.compareResults(results.get(command.processId)
                                                                                   .map(r -> List.of(r, command.result))
                                                                                   .getOrElseThrow(RuntimeException::new));
            printResult(handResult.withCCI(this.handsAndBoard), fileOutputName);
        }
        return this;
    }

    private void printResult(HandResult result, String fileName) {
        final String output = result.toString();
        System.out.println(output);

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(fileName, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(output);
        printWriter.close();
    }

    public static class HandleResult {

        private final int processId;
        private final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> result;

        public HandleResult(int processId, Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> result) {
            this.processId = processId;
            this.result = result;
        }
    }

}
