package com.pokerstars.omaha.actors;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.pokerstars.omaha.comp.IHandProcessor;
import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Either;

public class HandProcessor extends AbstractBehavior<HandProcessor.ProcessCardCombination> {

    public HandProcessor(ActorContext<ProcessCardCombination> context) {
        super(context);
    }

    public static Behavior<ProcessCardCombination> create() {
        return Behaviors.setup(HandProcessor::new);
    }

    @Override
    public Receive<ProcessCardCombination> createReceive() {
        return newReceiveBuilder().onMessage(ProcessCardCombination.class, this::onProcessCardCombination).build();
    }

    private Behavior<ProcessCardCombination> onProcessCardCombination(ProcessCardCombination command) {

        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handResults = CardProcessorUtils.processHand(command.hand, command.board, command.handProcessors, command.lowHandProcessor);

        command.resultProcessor.tell(new ResultProcessor.HandleResult(command.processId, handResults));

        return this;
    }

    public static class ProcessCardCombination {

        private final int processId;
        private final CardCombination hand;
        private final CardCombination board;
        private final IHandProcessor lowHandProcessor;
        private final List<IHandProcessor> handProcessors;
        private final ActorRef<ResultProcessor.HandleResult> resultProcessor;

        public CardCombination getHand() {
            return hand;
        }

        public CardCombination getBoard() {
            return board;
        }

        public List<IHandProcessor> getHandProcessors() {
            return handProcessors;
        }

        public ActorRef<ResultProcessor.HandleResult> getResultProcessor() {
            return resultProcessor;
        }

        public IHandProcessor getLowHandProcessor() {
            return lowHandProcessor;
        }

        public ProcessCardCombination(CardCombination hand, CardCombination board, List<IHandProcessor> handProcessors, IHandProcessor lowHandProcessor, ActorRef<ResultProcessor.HandleResult> resultProcessor, int processId) {
            this.hand = hand;
            this.board = board;
            this.handProcessors = handProcessors;
            this.lowHandProcessor = lowHandProcessor;
            this.resultProcessor = resultProcessor;
            this.processId = processId;
        }
    }
}
