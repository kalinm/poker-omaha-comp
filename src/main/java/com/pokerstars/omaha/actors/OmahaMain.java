package com.pokerstars.omaha.actors;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.pokerstars.omaha.comp.FlushProcessor;
import com.pokerstars.omaha.comp.FourOfAKindProcessor;
import com.pokerstars.omaha.comp.FullHouseProcessor;
import com.pokerstars.omaha.comp.HighCardProcessor;
import com.pokerstars.omaha.comp.IHandProcessor;
import com.pokerstars.omaha.comp.Low8Processor;
import com.pokerstars.omaha.comp.OnePairProcessor;
import com.pokerstars.omaha.comp.StraightFlushProcessor;
import com.pokerstars.omaha.comp.StraightProcessor;
import com.pokerstars.omaha.comp.ThreeOfAKindProcessor;
import com.pokerstars.omaha.comp.TwoPairsProcessor;
import com.pokerstars.omaha.utils.InputParserUtils;
import io.vavr.collection.List;

public class OmahaMain extends AbstractBehavior<OmahaMain.EvaluateHands> {

    private final ActorRef<HandProcessor.ProcessCardCombination> handAProcessor;
    private final ActorRef<HandProcessor.ProcessCardCombination> handBProcessor;
    private static final IHandProcessor LOW_HAND_PROCESSOR = new Low8Processor();
    private static final List<IHandProcessor> HIGH_HAND_PROCESSORS = List.of(new StraightFlushProcessor(),
                                                                             new FourOfAKindProcessor(),
                                                                             new FullHouseProcessor(),
                                                                             new FlushProcessor(),
                                                                             new StraightProcessor(),
                                                                             new ThreeOfAKindProcessor(),
                                                                             new TwoPairsProcessor(),
                                                                             new OnePairProcessor(),
                                                                             new HighCardProcessor());
    private int processId = 0;

    public static Behavior<EvaluateHands> create() {
        return Behaviors.setup(OmahaMain::new);
    }

    public OmahaMain(ActorContext<EvaluateHands> context) {
        super(context);

        handAProcessor = context.spawn(HandProcessor.create(), "handAProcessor");
        handBProcessor = context.spawn(HandProcessor.create(), "handBProcessor");
    }

    @Override
    public Receive<EvaluateHands> createReceive() {
        return newReceiveBuilder().onMessage(EvaluateHands.class, this::onHandleFileInput).build();
    }

    private Behavior<EvaluateHands> onHandleFileInput(EvaluateHands command) {

        command.cardCombinations.map(InputParserUtils::parseInput)
                                .forEach(c -> {
                                    processId++;
                                    final ActorRef<ResultProcessor.HandleResult> resultProcessor = getContext().spawn(ResultProcessor.create(c, command.fileOutputName), "resultProcessor-" + processId);
                                    handAProcessor.tell(new HandProcessor.ProcessCardCombination(c.getHands().get(0), c.getBoard(), HIGH_HAND_PROCESSORS, LOW_HAND_PROCESSOR, resultProcessor, processId));
                                    handBProcessor.tell(new HandProcessor.ProcessCardCombination(c.getHands().get(1), c.getBoard(), HIGH_HAND_PROCESSORS, LOW_HAND_PROCESSOR, resultProcessor, processId));
                                });
        return this;
    }

    public static class EvaluateHands {
        final List<String> cardCombinations;
        final String fileOutputName;

        public EvaluateHands(List<String> cardCombinations, String fileOutputName) {
            this.cardCombinations = cardCombinations;
            this.fileOutputName = fileOutputName;
        }
    }
}
