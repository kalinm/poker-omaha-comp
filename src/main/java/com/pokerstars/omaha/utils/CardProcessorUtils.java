package com.pokerstars.omaha.utils;

import java.util.function.BiFunction;

import com.pokerstars.omaha.comp.IHandProcessor;
import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandResult;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;

public enum CardProcessorUtils {
    ;

    private static final BiFunction<QualifiedResult, QualifiedResult, QualifiedResult> highWinComparator = (a, b) -> {
        if (a.getHandRank() == b.getHandRank()) {
            // TODO Need to think about all cases
            switch (a.getHandRank()) {
                //case StraightFlush:
                case Straight:
                    return compareStraight(a, b);
                default:
                    return compareBySum(a, b);
            }
        }
        return a.getHandRank().getValue() >= b.getHandRank().getValue() ? a : b;
    };

    /**
     * low win comparator
     */
    private static final BiFunction<QualifiedResult, QualifiedResult, QualifiedResult> lowWinComparator = (a, b) -> {
        final Seq<Integer> aCards = prependAce(a.getCards())._1.map(Cards::getValue).sorted();
        final Seq<Integer> bCards = prependAce(b.getCards())._1.map(Cards::getValue).sorted();

        for (int i = 0; i < aCards.size(); i++) {
            if (aCards.get(i) < bCards.get(i)) {
                return a.withSplit(false);
            } else if (bCards.get(i) < aCards.get(i)) {
                return b.withSplit(false);
            }
        }

        return a.withSplit(true);
    };

    public static QualifiedResult compareStraight(QualifiedResult a, QualifiedResult b) {
        final Seq<Cards> aSorted = a.getCards().sortBy(Cards::getValue).reverse();
        final Seq<Cards> bSorted = b.getCards().sortBy(Cards::getValue).reverse();

        final boolean isARoyal = aSorted.get(0).getValue() == 14 && aSorted.get(1).getValue() == 13;
        final boolean isBRoyal = bSorted.get(0).getValue() == 14 && bSorted.get(1).getValue() == 13;

        if (isARoyal && isBRoyal) {
            return a.withSplit(true);
        }

        if (isARoyal) {
            return a.withSplit(false);
        }

        if (isBRoyal) {
            return b.withSplit(false);
        }

        final boolean isAce1A = aSorted.get(0).getValue() == 14;
        final boolean isAce1B = bSorted.get(0).getValue() == 14;

        if (isAce1A && isAce1B) {
            return a.withSplit(true);
        }

        if (isAce1A) {
            return b.withSplit(false);
        }

        if (isAce1B) {
            return a.withSplit(false);
        }

        return compareBySum(a, b);
    }


    public static QualifiedResult compareBySum(QualifiedResult a, QualifiedResult b) {
        final Integer aSum = a.getCards().map(Cards::getValue).reduce(Integer::sum);
        final Integer bSum = b.getCards().map(Cards::getValue).reduce(Integer::sum);

        if (aSum > bSum) {
            return a;
        }

        if (bSum > aSum) {
            return b;
        }

        return a.withSplit(true);
    }

    public static Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> processHand(CardCombination hand, CardCombination board, List<IHandProcessor> hiProcessors, IHandProcessor loProcessor) {
        final List<Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>>> results = CardProcessorUtils.getCombinations(hand, board).map(cc -> {
            Either<NotQualifiedResult, QualifiedResult> highResult = CardProcessorUtils.createInitResult(cc.getId());
            Either<NotQualifiedResult, QualifiedResult> lowResult = CardProcessorUtils.createInitResult(cc.getId());

            for (IHandProcessor handProcessor : hiProcessors) {
                highResult = handProcessor.processHand(cc);
                lowResult = loProcessor.processHand(cc);
                if (highResult.isRight()) {
                    break;
                }
            }
            return Tuple.of(highResult, lowResult);
        });

        return CardProcessorUtils.evaluateResults(results, hand.getId());
    }

    /**
     * Calculates all possible variants combining 2 cards from hand with 3 cards from board
     *
     * @param hand  The user's 4 card combination
     * @param board The 5 cards on board
     * @return a List of all possible combinations 2 cards from hand x 3 cards from board
     */
    public static List<CardCombination> getCombinations(CardCombination hand, CardCombination board) {

        final List<List<Cards>> combinationsH = hand.getCards().toList().combinations(2).filter(c -> c.size() == 2);
        final List<List<Cards>> combinationsB = board.getCards().toList().combinations(3).filter(c -> c.size() == 3);

        return combinationsH.flatMap(h -> combinationsB.map(h::appendAll))
                            .map(l -> CardCombination.builder().combinationType(CombinationType.Combined).id(hand.getId()).cards(l).build());
    }

    public static HandResult compareResults(List<Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>>> results) {

        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handResults = evaluateResults(results, "");

        final HandResult.HandResultBuilder handResultBuilder = HandResult.builder();
        handResults._1.map(q -> handResultBuilder.hiWinId(q.getHandId())
                                                 .hiWin(q.getHandRank())
                                                 .hiCards(q.getCards())
                                                 .isHiSplit(q.isSplit())
                                                 .build());
        handResults._2.map(q -> handResultBuilder.lowWinId(q.getHandId())
                                                 .lowWin(q.getHandRank())
                                                 .lowCards(q.getCards())
                                                 .isLowSplit(q.isSplit())
                                                 .build());

        return handResultBuilder.build();
    }

    public static Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> evaluateResults(List<Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>>> results, String handId) {

        final Either<NotQualifiedResult, QualifiedResult> highWin = results.map(Tuple2::_1)
                                                                           .filter(Either::isRight)
                                                                           .map(Either::get)
                                                                           .reduceOption(highWinComparator)
                                                                           .map(Either::<NotQualifiedResult, QualifiedResult>right)
                                                                           .getOrElse(Either.left(toNotQualifiedResult(handId, HandType.HighHand, HandRank.NotSet)));

        final Either<NotQualifiedResult, QualifiedResult> lowWin = results.map(Tuple2::_2)
                                                                          .filter(Either::isRight)
                                                                          .map(Either::get)
                                                                          .reduceOption(lowWinComparator)
                                                                          .map(Either::<NotQualifiedResult, QualifiedResult>right)
                                                                          .getOrElse(Either.left(toNotQualifiedResult(handId, HandType.Low8Hand, HandRank.Low8)));

        return Tuple.of(highWin, lowWin);
    }

    public static Either<NotQualifiedResult, QualifiedResult> createInitResult(String handId) {
        return Either.left(NotQualifiedResult.builder().handId(handId).handRank(HandRank.NotSet).handType(HandType.NotSet).build());
    }

    public static QualifiedResult toQualifiedResult(String handId, HandType handType, HandRank handRank, Seq<Cards> cards) {
        return QualifiedResult.builder()
                              .handId(handId)
                              .handType(handType)
                              .handRank(handRank)
                              .cards(cards)
                              .build();
    }

    public static NotQualifiedResult toNotQualifiedResult(String handId, HandType handType, HandRank handRank) {
        return NotQualifiedResult.builder()
                                 .handId(handId)
                                 .handType(handType)
                                 .handRank(handRank)
                                 .build();
    }

    public static Seq<Seq<Cards>> findConsecutiveCards(Seq<Cards> cards, int numCards) {
        return cards.map(c -> cards.sortBy(Cards::getValue).scan(c, (a, b) -> a.getValue() == b.getValue() - 1 ? b : a).distinct()).filter(s -> s.size() == numCards);
    }


    public static Option<Seq<Cards>> find5ConsecutiveCardsHigh(Seq<Cards> cards) {
        return findConsecutiveCards(cards, 5).reduceOption((a, b) -> a.last().getValue() > b.last().getValue() ? a : b);
    }

    public static Tuple2<Seq<Cards>, Option<Cards>> prependAce(Seq<Cards> cards) {
        final Option<Cards> ace = cards.find(c -> c.getValue() == 14);
        return ace.map(a -> Tuple.of(cards.prepend(Cards.fromSuitAndValue(a.getSuit(), 1)), ace)).getOrElse(Tuple.of(cards, ace));
    }

    public static Seq<Cards> replaceTempAce(Seq<Cards> cards, Option<Cards> ace) {
        return ace.map(a -> cards.find(c -> c.getValue() == 1)
                                 .map(at -> cards.replace(at, a))
                                 .getOrElse(cards)).getOrElse(cards);
    }

    public static Option<? extends Tuple2<Integer, ? extends Seq<Cards>>> findNOfAKind(Seq<Cards> cards, int numCards) {
        return cards.groupBy(Cards::getValue)
                    .filter(t -> t._2.size() == numCards)
                    .reduceOption((ta, tb) -> ta._1 > tb._1 ? ta : tb);
    }
}
