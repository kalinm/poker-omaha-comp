package com.pokerstars.omaha.utils;

import java.util.regex.Pattern;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.CardCombinationsInput;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import io.vavr.Tuple2;
import io.vavr.collection.List;

public enum InputParserUtils {
    ;

    public static CardCombinationsInput parseInput(String input) {
        final List<CardCombination> cardCombinations = parseTable(input);
        final Tuple2<List<CardCombination>, List<CardCombination>> handsAndBoard = cardCombinations.splitAt(c -> c.getCombinationType().equals(CombinationType.Board));
        return new CardCombinationsInput(handsAndBoard._1, handsAndBoard._2.head(), input);
    }

    //HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-6d Board:Ad-Kh-5s-2d-Qd
    static List<CardCombination> parseTable(String input) {
        return Pattern.compile(" ")
                      .splitAsStream(input)
                      .map(InputParserUtils::parseCombination)
                      .collect(List.collector());
    }

    //HandA:Ac-Kd-Jd-3d
    static CardCombination parseCombination(String combination) {
        final String[] data = Pattern.compile(":").split(combination);
        final List<Cards> cards = Pattern.compile("-").splitAsStream(data[1]).map(Cards::fromName).collect(List.collector());
        final CombinationType combinationType = cards.size() == 5 ? CombinationType.Board : CombinationType.Hand;

        return CardCombination.builder()
                              .combinationType(combinationType)
                              .id(data[0])
                              .cards(cards)
                              .build();
    }

}
