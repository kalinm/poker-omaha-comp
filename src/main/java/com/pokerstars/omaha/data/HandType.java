package com.pokerstars.omaha.data;

public enum HandType {
    HighHand, Low8Hand, NotSet
}
