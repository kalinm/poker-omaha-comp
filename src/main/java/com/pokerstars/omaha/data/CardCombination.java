package com.pokerstars.omaha.data;

import io.vavr.collection.Seq;
import lombok.Builder;
import lombok.Value;

/*
HandB:card1-card2-card3-card4
 */
@Value
@Builder
public class CardCombination {

    CombinationType combinationType;
    String id;
    Seq<Cards> cards;
}
