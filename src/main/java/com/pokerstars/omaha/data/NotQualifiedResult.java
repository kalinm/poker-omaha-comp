package com.pokerstars.omaha.data;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class NotQualifiedResult {

    String handId;
    HandType handType;
    HandRank handRank;

}
