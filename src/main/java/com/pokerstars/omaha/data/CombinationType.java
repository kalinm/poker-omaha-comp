package com.pokerstars.omaha.data;

public enum CombinationType {
    Hand, Board, Combined
}
