package com.pokerstars.omaha.data;

public enum HandRank {
    StraightFlush(10),
    FourOfAKind(9),
    FullHouse(8),
    Flush(7),
    Straight(6),
    ThreeOfAKind(5),
    TwoPair(4),
    OnePair(3),
    HighCard(2),
    Low8(1),
    NotSet(0);

    private final int value;

    public int getValue() {
        return value;
    }

    HandRank(int value) {
        this.value = value;
    }
}
