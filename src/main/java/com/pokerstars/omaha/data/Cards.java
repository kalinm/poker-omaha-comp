package com.pokerstars.omaha.data;

public enum Cards {
    Ad(Suit.D, "Ad", 14), Ac(Suit.C, "Ac", 14), Ah(Suit.H, "Ah", 14), As(Suit.S, "As", 14),
    Kd(Suit.D, "Kd", 13), Kc(Suit.C, "Kc", 13), Kh(Suit.H, "Kh", 13), Ks(Suit.S, "Ks", 13),
    Qd(Suit.D, "Qd", 12), Qc(Suit.C, "Qc", 12), Qh(Suit.H, "Qh", 12), Qs(Suit.S, "Qs", 12),
    Jd(Suit.D, "Jd", 11), Jc(Suit.C, "Jc", 11), Jh(Suit.H, "Jh", 11), Js(Suit.S, "Js", 11),
    Td(Suit.D, "Td", 10), Tc(Suit.C, "Tc", 10), Th(Suit.H, "Th", 10), Ts(Suit.S, "Ts", 10),
    N9d(Suit.D, "9d", 9), N9c(Suit.C, "9c", 9), N9h(Suit.H, "9h", 9), N9s(Suit.S, "9s", 9),
    N8d(Suit.D, "8d", 8), N8c(Suit.C, "8c", 8), N8h(Suit.H, "8h", 8), N8s(Suit.S, "8s", 8),
    N7d(Suit.D, "7d", 7), N7c(Suit.C, "7c", 7), N7h(Suit.H, "7h", 7), N7s(Suit.S, "7s", 7),
    N6d(Suit.D, "6d", 6), N6c(Suit.C, "6c", 6), N6h(Suit.H, "6h", 6), N6s(Suit.S, "6s", 6),
    N5d(Suit.D, "5d", 5), N5c(Suit.C, "5c", 5), N5h(Suit.H, "5h", 5), N5s(Suit.S, "5s", 5),
    N4d(Suit.D, "4d", 4), N4c(Suit.C, "4c", 4), N4h(Suit.H, "4h", 4), N4s(Suit.S, "4s", 4),
    N3d(Suit.D, "3d", 3), N3c(Suit.C, "3c", 3), N3h(Suit.H, "3h", 3), N3s(Suit.S, "3s", 3),
    N2d(Suit.D, "2d", 2), N2c(Suit.C, "2c", 2), N2h(Suit.H, "2h", 2), N2s(Suit.S, "2s", 2),
    //The aces bellow is manually added for proper evaluation
    ALd(Suit.D, "ALd", 1), ALc(Suit.C, "ALc", 1), ALh(Suit.H, "ALh", 1), ALs(Suit.S, "ALs", 1);

    private Suit suit;
    private String name;
    private int value;

    public int getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }

    Cards(Suit suit, String name, int value) {
        this.suit = suit;
        this.name = name;
        this.value = value;
    }

    public static Cards fromName(String str) {
        for (final Cards c : values()) {
            if (str.toLowerCase().equals(c.name.toLowerCase())) {
                return c;
            }
        }

        throw new IllegalArgumentException("Invalid Card name");
    }

    public static Cards fromSuitAndValue(Suit suit, int value) {
        for (final Cards c : values()) {
            if (c.getSuit().equals(suit) && c.value == value) {
                return c;
            }
        }

        throw new IllegalArgumentException("No card found for the given value and suit");
    }
}

