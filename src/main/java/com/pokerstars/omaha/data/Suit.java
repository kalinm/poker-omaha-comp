package com.pokerstars.omaha.data;

public enum Suit {
    D, C, H, S
}
