package com.pokerstars.omaha.data;

import io.vavr.collection.List;
import lombok.Value;

@Value
public class CardCombinationsInput {

    List<CardCombination> hands;
    CardCombination board;
    String rawInput;

}
