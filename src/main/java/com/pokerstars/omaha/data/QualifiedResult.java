package com.pokerstars.omaha.data;

import io.vavr.collection.Seq;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class QualifiedResult {
    String handId;
    HandType handType;
    HandRank handRank;
    Seq<Cards> cards;
    boolean isSplit;

    public QualifiedResult withSplit(boolean isSplit) {
        return new QualifiedResult(handId, handType, handRank, cards, isSplit);
    }
}
