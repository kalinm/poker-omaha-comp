package com.pokerstars.omaha.data;

import io.vavr.collection.Seq;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class HandResult {

    String hiWinId;
    HandRank hiWin;
    Seq<Cards> hiCards;
    Boolean isHiSplit;

    String lowWinId;
    HandRank lowWin;
    Seq<Cards> lowCards;
    Boolean isLowSplit;

    CardCombinationsInput cci;

    public HandResult withCCI(CardCombinationsInput cci) {
        return new HandResult(hiWinId, hiWin, hiCards, isHiSplit, lowWinId, lowWin, lowCards, isLowSplit, cci);
    }


    // Template
    // HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-7d Board:Ah-Kh-5s-2s-Qd => HandB wins Hi (3-of-a-Kind); HandB wins Lo (7652A)
    // HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-6d Board:Ad-Kh-5s-2d-Qd => HandA wins Hi (Flush); No hand qualified for Low
    // HandA:Qc-Jd-Td-3d HandB:Tc-Jc-8h-6d Board:Ad-Kh-Qs-2d-3c => Split Pot Hi (Straight); HandB wins Lo (8632A)
    // HandA:Qh-4d-Tc-8s HandB:Qc-8c-7d-2h Board:Ad-As-3c-3d-5d => HandA wins Hi (One Pair); HandB wins Lo (7532A)
    // HandA:Ah-2s-Qd-9S HandB:Ac-2d-As-Jh Board:Kd-4h-Kh-5s-3c => Split Pot Hi (Straight); Split Pot Lo (5432A)

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(cci != null ? cci.getRawInput() : "Raw input is missing!").append(" => ");

        if (Boolean.TRUE.equals(isHiSplit)) {
            builder.append("Split Pot Hi (").append(hiWin).append("); ");
        } else {
            builder.append(hiWinId).append(" wins Hi ").append("(").append(hiWin).append("); ");
        }

        if (lowCards != null) {
            if (Boolean.TRUE.equals(isLowSplit)) {
               builder.append("Split Pot Lo").append(formatCards(lowCards));
            } else {
                builder.append(lowWinId).append(" wins Lo ").append(formatCards(lowCards));
            }
        } else {
            builder.append("No hand qualified for Low");
        }

        return builder.toString();
    }

    private String formatCards(Seq<Cards> cards) {
        final Seq<Cards> cardsSorted = cards.sortBy(Cards::getValue).reverse();

        if (cardsSorted.head().getValue() == 14 && cardsSorted.last().getValue() == 2) {
            return "(" + cardsSorted.remove(cardsSorted.get(0)).append(cardsSorted.get(0)).map(this::formatCardName).mkString() + ")";
        }
        return "(" + cardsSorted.map(this::formatCardName).mkString() + ")";
    }

    private String formatCardName(Cards card) {
        if (card.getValue() == 10) {
            return "10";
        } else if (card.getValue() < 10) {
            return Character.toString(card.toString().charAt(1));
        }

        return Character.toString(card.toString().charAt(0));
    }
}
