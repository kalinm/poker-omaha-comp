package com.pokerstars.omaha;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import akka.actor.typed.ActorSystem;
import com.pokerstars.omaha.actors.OmahaMain;
import io.vavr.collection.List;

public class OmahaComp {

    public static void main(String[] args) {

        final String fileInputName = args.length > 0 ? args[0] : "input.txt";
        final String fileOutputName = args.length > 1 ? args[1] : "output.txt";

        final List<String> cardCombinations;

        try {
            cardCombinations = List.ofAll(Files.readAllLines(Paths.get(fileInputName)));
        } catch (IOException e) {
            System.out.println("Unable to load file with file name: " + fileInputName);
            e.printStackTrace();
            return;
        }

        final ActorSystem<OmahaMain.EvaluateHands> omahaMain = ActorSystem.create(OmahaMain.create(), "omahacomp");

        omahaMain.tell(new OmahaMain.EvaluateHands(cardCombinations, fileOutputName));

        try {
            System.out.println(">>> Evaluating + " + cardCombinations.size() + " bets. Please find the results in " + fileOutputName);
            System.out.println(">>> Press ENTER to exit <<<");
            System.in.read();
        } catch (IOException ignored) {
        } finally {
            omahaMain.terminate();
        }
    }
}
