package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.control.Either;

public class HighCardProcessor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.HighCard;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {
        // if reached this processor that means no other combination matched so only check cards > 8
        final boolean allHigh = cc.getCards().forAll(c -> c.getValue() > 8);

        return allHigh ? Either.right(CardProcessorUtils.toQualifiedResult(cc.getId(), this.getType(), this.getRank(), cc.getCards().sortBy(Cards::getValue))) :
               Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank()));
    }
}
