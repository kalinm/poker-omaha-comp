package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.collection.Seq;
import io.vavr.control.Either;

public class TwoPairsProcessor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.TwoPair;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {

        final Seq<Cards> pairs = cc.getCards()
                                   .groupBy(Cards::getValue)
                                   .filter(t -> t._2.size() == 2)
                                   .values()
                                   .flatMap(s -> s);

        if (!pairs.isEmpty() && pairs.size() == 4) {
            return Either.right(CardProcessorUtils.toQualifiedResult(cc.getId(), this.getType(), this.getRank(), pairs.appendAll(cc.getCards().filter(c -> !pairs.contains(c)))));
        }

        return Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank()));
    }
}
