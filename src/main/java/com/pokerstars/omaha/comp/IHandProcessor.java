package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.control.Either;

public interface IHandProcessor {
    HandRank getRank();
    HandType getType();
    Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc);
}
