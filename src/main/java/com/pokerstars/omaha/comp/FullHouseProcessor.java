package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Either;
import io.vavr.control.Option;

public class FullHouseProcessor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.FullHouse;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {

        final Map<Integer, List<Cards>> cards = cc.getCards()
                                                  .groupBy(Cards::getValue)
                                                  .mapValues(List::ofAll);

        final Option<Tuple2<Integer, List<Cards>>> threeOfAK = cards.filter(t -> t._2.size() == 3).headOption();
        final Option<Tuple2<Integer, List<Cards>>> twoOfAK = cards.filter(t -> t._2.size() == 2).headOption();

        if (threeOfAK.isDefined() && twoOfAK.isDefined()) {
            return Either.right(CardProcessorUtils.toQualifiedResult(cc.getId(), this.getType(), this.getRank(), threeOfAK.get()._2.appendAll(twoOfAK.get()._2.subSequence(0, 2))));
        }

        return Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank()));
    }
}
