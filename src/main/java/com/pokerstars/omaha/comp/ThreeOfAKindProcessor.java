package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.HandRank;

public class ThreeOfAKindProcessor extends NOfAKindProcessor {

    public ThreeOfAKindProcessor() {
        super(3, HandRank.ThreeOfAKind);
    }
}
