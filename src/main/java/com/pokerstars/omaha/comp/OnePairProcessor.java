package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.HandRank;

public class OnePairProcessor extends NOfAKindProcessor {

    public OnePairProcessor() {
        super(2, HandRank.OnePair);
    }
}
