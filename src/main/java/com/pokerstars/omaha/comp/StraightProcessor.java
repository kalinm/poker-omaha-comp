package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.Tuple2;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;

public class StraightProcessor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.Straight;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {
        // prepend a temporary ace
        final Tuple2<Seq<Cards>, Option<Cards>> cardsAndAce = CardProcessorUtils.prependAce(cc.getCards());

        return CardProcessorUtils.find5ConsecutiveCardsHigh(cardsAndAce._1).map(s -> CardProcessorUtils.replaceTempAce(s, cardsAndAce._2))
                                 // result success
                                 .map(s -> Either.<NotQualifiedResult, QualifiedResult>right(CardProcessorUtils.toQualifiedResult(cc.getId(), HandType.HighHand, this.getRank(), s)))
                                 // result N/A
                                 .getOrElse(Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank())));
    }
}
