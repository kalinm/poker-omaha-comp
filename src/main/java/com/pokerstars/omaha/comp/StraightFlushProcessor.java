package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.data.Suit;
import io.vavr.Function2;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;

import static com.pokerstars.omaha.utils.CardProcessorUtils.*;

/**
 * This processor evaluates the cards on hand and on board to find a consecutive sequence of cards with the same suit
 * a.k.a Straight Flush
 * For example 8h,9h,Th,Jh,Qh or As,2s,3s,4s,5s or Tc,Jc,Qc,Kc,Ac
 */
public final class StraightFlushProcessor implements IHandProcessor {

    private static Function2<Suit, Seq<Cards>, Tuple2<Suit, Seq<Cards>>> consecutive = (suit, cards) -> Tuple.of(suit, find5ConsecutiveCardsHigh(cards).getOrElse(List.empty()));

    @Override
    public HandRank getRank() {
        return HandRank.StraightFlush;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {
        // append board cards to hand cards and prepend a temporary ace
        final Tuple2<Seq<Cards>, Option<Cards>> cardsAndAce = prependAce(cc.getCards());

        return cardsAndAce._1.groupBy(Cards::getSuit)
                             //For each suit go check if cards are consecutive
                             .map(consecutive)
                             //check if 5 consecutive cards found
                             .find(t -> t._2.size() == 5)
                             //get cards
                             .map(Tuple2::_2)
                             // replace the temp ace with the actual one (if any)
                             .map(s -> replaceTempAce(s, cardsAndAce._2))
                             // result success
                             .map(s -> Either.<NotQualifiedResult, QualifiedResult>right(toQualifiedResult(cc.getId(), this.getType(), this.getRank(), s)))
                             // result N/A
                             .getOrElse(Either.left(toNotQualifiedResult(cc.getId(), this.getType(), this.getRank())));
    }
}
