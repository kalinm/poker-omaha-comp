package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.HandRank;

public class FourOfAKindProcessor extends NOfAKindProcessor {

    public FourOfAKindProcessor() {
        super(4, HandRank.FourOfAKind);
    }
}
