package com.pokerstars.omaha.comp;
import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.data.Suit;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;

import static com.pokerstars.omaha.utils.CardProcessorUtils.toNotQualifiedResult;
import static com.pokerstars.omaha.utils.CardProcessorUtils.toQualifiedResult;

public class FlushProcessor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.Flush;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {

        final Option<Tuple2<Suit, List<Cards>>> cards = cc.getCards()
                                                          .sortBy(Cards::getValue)
                                                          .reverse()
                                                          .groupBy(Cards::getSuit)
                                                          .mapValues(List::ofAll)
                                                          .filter(t -> t._2.size() == 5)
                                                          .headOption();
        return cards.map(Tuple2::_2)
                    .map(s -> Either.<NotQualifiedResult, QualifiedResult>right(toQualifiedResult(cc.getId(), this.getType(), this.getRank(), s)))
                    .getOrElse(Either.left(toNotQualifiedResult(cc.getId(), this.getType(), this.getRank())));
    }
}
