package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.control.Either;

public class Low8Processor implements IHandProcessor {

    @Override
    public HandRank getRank() {
        return HandRank.Low8;
    }

    @Override
    public HandType getType() {
        return HandType.Low8Hand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {
        final boolean isLow8Hand = cc.getCards().filter(c -> c.getValue() == 14 || c.getValue() < 9).map(Cards::getValue).distinct().size() == 5;

        return isLow8Hand ? Either.right(CardProcessorUtils.toQualifiedResult(cc.getId(), this.getType(), this.getRank(), cc.getCards().sortBy(Cards::getValue))) :
               Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank()));
    }
}
