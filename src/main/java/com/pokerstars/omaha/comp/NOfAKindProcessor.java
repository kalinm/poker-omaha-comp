package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.CardProcessorUtils;
import io.vavr.Tuple2;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import io.vavr.control.Option;

public class NOfAKindProcessor implements IHandProcessor {

    private final int nOAK;
    private final HandRank rank;

    public NOfAKindProcessor(int nOAK, HandRank rank) {
        this.nOAK = nOAK;
        this.rank = rank;
    }

    @Override
    public HandRank getRank() {
        return rank;
    }

    @Override
    public HandType getType() {
        return HandType.HighHand;
    }

    @Override
    public Either<NotQualifiedResult, QualifiedResult> processHand(CardCombination cc) {

        final Option<? extends Tuple2<Integer, ? extends Seq<Cards>>> cards = cc.getCards()
                                                                                .groupBy(Cards::getValue)
                                                                                .filter(t -> t._2.size() == nOAK)
                                                                                .reduceOption((ta, tb) -> ta._1 > tb._1 ? ta : tb);
        return cards.map(t -> t._2.appendAll(cc.getCards().filter(c -> !t._2.contains(c))))
                    .map(s -> Either.<NotQualifiedResult, QualifiedResult>right(CardProcessorUtils.toQualifiedResult(cc.getId(), this.getType(), this.getRank(), s)))
                    .getOrElse(Either.left(CardProcessorUtils.toNotQualifiedResult(cc.getId(), this.getType(), this.getRank())));
    }
}
