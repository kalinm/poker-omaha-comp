package com.pokerstars.omaha;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class TempTest {

    @Test
    public void givenWritingStringToFile_whenUsingPrintWriter_thenCorrect() throws IOException {
        FileWriter fileWriter = new FileWriter("output.txt", true);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println("Product name");
        printWriter.close();
    }
}
