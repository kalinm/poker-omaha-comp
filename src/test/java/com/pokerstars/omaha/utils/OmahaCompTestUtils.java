package com.pokerstars.omaha.utils;

import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.Seq;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public enum OmahaCompTestUtils {
    ;

    public static void validateCombination(QualifiedResult result, HandRank expectedRank, HandType expectedType, String expectedId, Seq<Cards> expectedCards) {
        assertEquals(expectedRank, result.getHandRank());
        assertEquals(expectedType, result.getHandType());
        assertEquals(expectedId, result.getHandId());
        expectedCards.forEach(c -> assertTrue(result.getCards().contains(c)));
    }
}
