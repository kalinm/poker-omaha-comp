package com.pokerstars.omaha.utils;

import com.pokerstars.omaha.comp.FlushProcessor;
import com.pokerstars.omaha.comp.FourOfAKindProcessor;
import com.pokerstars.omaha.comp.FullHouseProcessor;
import com.pokerstars.omaha.comp.HighCardProcessor;
import com.pokerstars.omaha.comp.IHandProcessor;
import com.pokerstars.omaha.comp.Low8Processor;
import com.pokerstars.omaha.comp.OnePairProcessor;
import com.pokerstars.omaha.comp.StraightFlushProcessor;
import com.pokerstars.omaha.comp.StraightProcessor;
import com.pokerstars.omaha.comp.ThreeOfAKindProcessor;
import com.pokerstars.omaha.comp.TwoPairsProcessor;
import com.pokerstars.omaha.data.CardCombinationsInput;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandResult;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Either;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardProcessorUtilsTest {

    private static final IHandProcessor LOW_PROCESSOR = new Low8Processor();
    private static final List<IHandProcessor> PROCESSORS = List.of(new StraightFlushProcessor(),
                                                                   new FourOfAKindProcessor(),
                                                                   new FullHouseProcessor(),
                                                                   new FlushProcessor(),
                                                                   new StraightProcessor(),
                                                                   new ThreeOfAKindProcessor(),
                                                                   new TwoPairsProcessor(),
                                                                   new OnePairProcessor(),
                                                                   new HighCardProcessor());

    // HandA:Ah-2s-Qd-9S HandB:Ac-2d-As-Jh Board:Kd-4h-Kh-5s-3 => Split Pot Hi (Straight); Split Pot Lo (5432A)
    @Test
    public void testSplitHiAndLoPot() {
        final String input = "HandA:Ah-2s-Qd-9S HandB:Ac-2d-As-Jh Board:Kd-4h-Kh-5s-3c";

        final CardCombinationsInput cardCombinationsInput = InputParserUtils.parseInput(input);

        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handAResult = CardProcessorUtils.processHand(cardCombinationsInput.getHands().get(0), cardCombinationsInput.getBoard(), PROCESSORS, LOW_PROCESSOR);
        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handBResult = CardProcessorUtils.processHand(cardCombinationsInput.getHands().get(1), cardCombinationsInput.getBoard(), PROCESSORS, LOW_PROCESSOR);

        final HandResult handResult = CardProcessorUtils.compareResults(List.of(handAResult, handBResult));

        assertTrue(handResult.getIsHiSplit());
        assertTrue(handResult.getIsLowSplit());
        assertEquals(HandRank.Straight, handResult.getHiWin());
    }

    /*
     * HandA:Qh-4d-Tc-8s HandB:Qc-8c-7d-2h Board:Ad-As-3c-3d-5d
     * HandA:Qh-4d-Tc-8s HandB:Qc-8c-7d-2h Board:Ad-As-3c-3d-5d => HandA wins Hi (One Pair); HandB wins Lo (7532A)
     */
    @Test
    public void testOnePairSplitLoPot() {
        final String input = "HandA:Qh-4d-Tc-8s HandB:Qc-8c-7d-2h Board:Ad-As-3c-3d-5d";

        final CardCombinationsInput cardCombinationsInput = InputParserUtils.parseInput(input);

        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handAResult = CardProcessorUtils.processHand(cardCombinationsInput.getHands().get(0), cardCombinationsInput.getBoard(), PROCESSORS, LOW_PROCESSOR);
        final Tuple2<Either<NotQualifiedResult, QualifiedResult>, Either<NotQualifiedResult, QualifiedResult>> handBResult = CardProcessorUtils.processHand(cardCombinationsInput.getHands().get(1), cardCombinationsInput.getBoard(), PROCESSORS, LOW_PROCESSOR);

        final HandResult handResult = CardProcessorUtils.compareResults(List.of(handAResult, handBResult));

        System.out.println(handResult.withCCI(cardCombinationsInput));

        assertFalse(handResult.getIsHiSplit());
        assertFalse(handResult.getIsLowSplit());
        assertEquals(HandRank.OnePair, handResult.getHiWin());
    }

    @Test
    public void find5ConsecutiveCardsTest() {
        final List<Cards> cards = List.of(Cards.N2s, Cards.N3s, Cards.N4s, Cards.N5s, Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        final Seq<Cards> consecutiveCardsHigh = CardProcessorUtils.find5ConsecutiveCardsHigh(cards).get();
        final List<Cards> expected = List.of(Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);

        assertEquals(5, consecutiveCardsHigh.size());
        consecutiveCardsHigh.forEach(c -> assertTrue(expected.contains(c)));
    }

    @Test
    public void find4ConsecutiveCardsTest() {
        final List<Cards> cards = List.of(Cards.N2s, Cards.N3s, Cards.N4s, Cards.N5s, Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        final Seq<Seq<Cards>> consecutiveCards = CardProcessorUtils.findConsecutiveCards(cards, 4);
        final List<Cards> expectedLow = List.of(Cards.N2s, Cards.N3s, Cards.N4s, Cards.N5s);
        final List<Cards> expectedHigh = List.of(Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);

        assertEquals(2, consecutiveCards.size());
        assertEquals(4, consecutiveCards.get(0).size());
        assertEquals(4, consecutiveCards.get(1).size());
        consecutiveCards.get(0).forEach(c -> assertTrue(expectedLow.contains(c)));
        consecutiveCards.get(1).forEach(c -> assertTrue(expectedHigh.contains(c)));
    }

    @Test
    public void find3ConsecutiveCardsTest() {
        final List<Cards> cards = List.of(Cards.N2s, Cards.N3s, Cards.N4s, Cards.N6s, Cards.N7s, Cards.N8s, Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        final Seq<Seq<Cards>> consecutiveCards = CardProcessorUtils.findConsecutiveCards(cards, 3);
        final List<Cards> expectedLow = List.of(Cards.N2s, Cards.N3s, Cards.N4s);
        final List<Cards> expectedMid = List.of(Cards.N6s, Cards.N7s, Cards.N8s);
        final List<Cards> expectedHigh = List.of(Cards.Qd, Cards.Kd, Cards.Ad);

        assertEquals(3, consecutiveCards.size());
        assertEquals(3, consecutiveCards.get(0).size());
        assertEquals(3, consecutiveCards.get(1).size());
        assertEquals(3, consecutiveCards.get(2).size());
        consecutiveCards.get(0).forEach(c -> assertTrue(expectedLow.contains(c)));
        consecutiveCards.get(1).forEach(c -> assertTrue(expectedMid.contains(c)));
        consecutiveCards.get(2).forEach(c -> assertTrue(expectedHigh.contains(c)));
    }

    @Test
    public void noConsecutiveCardsTest() {
        final List<Cards> cards = List.of(Cards.N8d, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        assertFalse(CardProcessorUtils.find5ConsecutiveCardsHigh(cards).isDefined());
    }

    @Test
    public void find3OAKind() {
        final List<Cards> cards = List.of(Cards.N2s, Cards.N2d, Cards.N2h, Cards.N5s, Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        final Tuple2<Integer, ? extends Seq<Cards>> threeOAK = CardProcessorUtils.findNOfAKind(cards, 3).get();
        final List<Cards> expected = List.of(Cards.N2s, Cards.N2d, Cards.N2h);

        assertEquals(3, threeOAK._2.size());
        threeOAK._2.forEach(c -> assertTrue(expected.contains(c)));
    }

    @Test
    public void no3OAKind() {
        final List<Cards> cards = List.of(Cards.N2s, Cards.N2d, Cards.N2h, Cards.N2c, Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);
        assertFalse(CardProcessorUtils.findNOfAKind(cards, 3).isDefined());
    }

}
