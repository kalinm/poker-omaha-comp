package com.pokerstars.omaha.utils;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.CardCombinationsInput;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import io.vavr.collection.List;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InputParserUtilsTest {

    @Test
    //HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-6d Board:Ad-Kh-5s-2d-Qd
    public void parseInput() {
        final CardCombinationsInput cardCombinationsInput = InputParserUtils.parseInput("HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-6d Board:Ad-Kh-5s-2d-Qd");

        validateCombination(cardCombinationsInput.getHands().get(0), CombinationType.Hand, "HandA", List.of(Cards.Ac, Cards.Kd, Cards.Jd, Cards.N3d));
        validateCombination(cardCombinationsInput.getHands().get(1), CombinationType.Hand, "HandB", List.of(Cards.N5c, Cards.N5d, Cards.N6c, Cards.N6d));
        validateCombination(cardCombinationsInput.getBoard(), CombinationType.Board, "Board", List.of(Cards.Ad, Cards.Kh, Cards.N5s, Cards.N2d, Cards.Qd));
    }

    @Test
    public void parseTableTest() {
        final String input = "HandA:Ac-Kd-Jd-3d HandB:5c-5d-6c-6d Board:Ad-Kh-5s-2d-Qd";

        final List<CardCombination> cardCombinations = InputParserUtils.parseTable(input);

        validateCombination(cardCombinations.get(0), CombinationType.Hand, "HandA", List.of(Cards.Ac, Cards.Kd, Cards.Jd, Cards.N3d));
        validateCombination(cardCombinations.get(1), CombinationType.Hand, "HandB", List.of(Cards.N5c, Cards.N5d, Cards.N6c, Cards.N6d));
        validateCombination(cardCombinations.get(2), CombinationType.Board, "Board", List.of(Cards.Ad, Cards.Kh, Cards.N5s, Cards.N2d, Cards.Qd));
    }

    @Test
    public void parseCombinationTest() {
        final String handCombination = "HandA:Ac-Kd-Jd-3d";

        final CardCombination cardCombination = InputParserUtils.parseCombination(handCombination);

        validateCombination(cardCombination, CombinationType.Hand, "HandA", List.of(Cards.Ac, Cards.Kd, Cards.Jd, Cards.N3d));
    }

    private void validateCombination(CardCombination cardCombination, CombinationType expectedType, String expectedId, List<Cards> expectedCards) {
        assertEquals(expectedType, cardCombination.getCombinationType());
        assertEquals(expectedId, cardCombination.getId());
        assertEquals(expectedCards.size(), cardCombination.getCards().size());
        expectedCards.forEach(c -> assertTrue(cardCombination.getCards().contains(c)));
    }

}
