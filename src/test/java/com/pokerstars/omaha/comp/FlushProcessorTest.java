package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Assert;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FlushProcessorTest {

    @Test
    public void testValidRank() {
        final IHandProcessor flushProcessor = new FlushProcessor();

        Assert.assertEquals(HandRank.Flush, flushProcessor.getRank());
    }

    @Test
    public void testFlushSpades() {
        final IHandProcessor flushProcessor = new FlushProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Combined)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Js, Cards.N4s, Cards.Ts, Cards.As, Cards.Qs))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = flushProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.As, Cards.Qs, Cards.Js, Cards.Ts, Cards.N4s);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.Flush, HandType.HighHand, "HandA", expectedCards);
    }

}
