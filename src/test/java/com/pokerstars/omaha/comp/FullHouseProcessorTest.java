package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FullHouseProcessorTest {

    @Test
    public void testFullHouse3xA2xK() {
        final IHandProcessor fullHouseProcessor = new FullHouseProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Ad, Cards.Kc, Cards.Ah, Cards.Kd, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = fullHouseProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ad, Cards.Ac, Cards.Ah, Cards.Kc, Cards.Kd);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.FullHouse, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testFullHouse3xK2xA() {
        final IHandProcessor fullHouseProcessor = new FullHouseProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.Ad, Cards.Kc, Cards.Ks, Cards.Kd, Cards.Ah))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = fullHouseProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Kc, Cards.Ks, Cards.Kd, Cards.Ad, Cards.Ah);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.FullHouse, HandType.HighHand, "HandA", expectedCards);
    }

}
