package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.OmahaCompTestUtils;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class Low8ProcessorTest {

    @Test
    public void testLow8WithAce() {
        final IHandProcessor low8Processor = new Low8Processor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandC")
                                                  .cards(List.of(Cards.N2c, Cards.N4s, Cards.N3c, Cards.N8h, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = low8Processor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N2c, Cards.N3c, Cards.N4s, Cards.N8h, Cards.Ac); // Ace at back.. ? where to format

        assertEquals(5, qualifiedResult.getCards().size());

        OmahaCompTestUtils.validateCombination(qualifiedResult, HandRank.Low8, HandType.Low8Hand, "HandC", expectedCards);
    }

}
