package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.*;

public class ThreeOfAKindProcessorTest {

    @Test
    public void testValidRank() {
        final IHandProcessor threeOfAKindProcessor = new ThreeOfAKindProcessor();

        assertEquals(HandRank.ThreeOfAKind, threeOfAKindProcessor.getRank());
    }

    @Test
    public void testFind3OfAKind() {
        final IHandProcessor threeOfAKindProcessor = new ThreeOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.N2d, Cards.N2c, Cards.N2s, Cards.Tc, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = threeOfAKindProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N2d, Cards.N2c, Cards.N2s, Cards.Tc, Cards.Ac);

        assertEquals(5, qualifiedResult.getCards().size()); // TODO see the comment in ThreeOfAKindProcessor

        validateCombination(qualifiedResult, HandRank.ThreeOfAKind, HandType.HighHand, "HandA", expectedCards);
    }

    /**
     * Here we have 3xJ + 3xA and expect 3 aces
     */
    @Test
    public void test3OfAKindX2() {
        final IHandProcessor threeOfAKindProcessor = new ThreeOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.As, Cards.Jd, Cards.Jc, Cards.Ad, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = threeOfAKindProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ad, Cards.Ac, Cards.As, Cards.Jd, Cards.Jc);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.ThreeOfAKind, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testNo3OfAKind() {
        final IHandProcessor threeOfAKindProcessor = new ThreeOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.N3d, Cards.N5c, Cards.N9c, Cards.N4s, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = threeOfAKindProcessor.processHand(cc);

        assertTrue(handResult.isEmpty());
    }

}
