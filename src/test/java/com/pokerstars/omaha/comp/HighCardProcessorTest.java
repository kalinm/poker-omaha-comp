package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.OmahaCompTestUtils;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HighCardProcessorTest {

    @Test
    public void testStraightHighAce() {
        final IHandProcessor highCardProcessor = new HighCardProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Qs, Cards.Kc, Cards.N9d, Cards.Jh, Cards.Ad))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = highCardProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N9d, Cards.Jh, Cards.Qs, Cards.Kc, Cards.Ad);

        assertEquals(5, qualifiedResult.getCards().size());

        OmahaCompTestUtils.validateCombination(qualifiedResult, HandRank.HighCard, HandType.HighHand, "HandA", expectedCards);
    }

}
