package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Assert;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.*;

public class OnePairProcessorTest {

    @Test
    public void testValidRank() {
        final IHandProcessor onePairProcessor = new OnePairProcessor();

        Assert.assertEquals(HandRank.OnePair, onePairProcessor.getRank());
    }

    @Test
    public void testFind2OfAKind() {
        final IHandProcessor onePairProcessor = new OnePairProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.N2d, Cards.N2s, Cards.N9c, Cards.Ac, Cards.N4s))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = onePairProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N2d, Cards.N2s, Cards.N9c, Cards.Ac, Cards.N4s);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.OnePair, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void test2OfAKindX2() {
        final IHandProcessor onePairProcessor = new OnePairProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Jd, Cards.Tc, Cards.N5d, Cards.Ad, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = onePairProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ad, Cards.Ac, Cards.Jd, Cards.Tc, Cards.N5d);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.OnePair, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testNo2OfAKind() {
        final IHandProcessor onePairProcessor = new OnePairProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.N3d, Cards.N5c, Cards.N9c, Cards.Ac))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = onePairProcessor.processHand(cc);

        assertTrue(handResult.isEmpty());
    }

}
