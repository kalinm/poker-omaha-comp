package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Assert;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.*;

public class StraightFlushProcessorTest {

    @Test
    public void validRank() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();

        Assert.assertEquals(HandRank.StraightFlush, straightFlushProcessor.getRank());
    }

    @Test
    public void testStraightFlushNoAce() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Jc, Cards.N8c, Cards.Qc, Cards.N9c, Cards.Tc))
                                                  .build();
        final Either<NotQualifiedResult, QualifiedResult> handResult = straightFlushProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N8c, Cards.N9c, Cards.Tc, Cards.Jc, Cards.Qc);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.StraightFlush, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testStraightFlushLowAce() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandC")
                                                  .cards(List.of(Cards.N2c, Cards.N5c, Cards.N3c, Cards.N4c, Cards.Ac))
                                                  .build();
        final Either<NotQualifiedResult, QualifiedResult> handResult = straightFlushProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ac, Cards.N2c, Cards.N3c, Cards.N4c, Cards.N5c);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.StraightFlush, HandType.HighHand, "HandC", expectedCards);
    }

    @Test
    public void testStraightFlushHighAce() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Td, Cards.Qd, Cards.Kd, Cards.Jd, Cards.Ad))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightFlushProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.StraightFlush, HandType.HighHand, "HandA", expectedCards);
    }

    /*// Here the ace could act both for high or low straight flush
    @Test
    public void testStraightFlush2PossibleStraights() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();
        final CardCombination hand = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandB")
                                                    .cards(List.of(Cards.N2d, Cards.N5d, Cards.N3d, Cards.Ad))
                                                    .build();
        final CardCombination board = CardCombination.builder()
                                                     .combinationType(CombinationType.Board).id("Board")
                                                     .cards(List.of(Cards.Qd, Cards.Td, Cards.Jd, Cards.Kd, Cards.N4d))
                                                     .build();
        final Either<NotQualifiedResult, QualifiedResult> handResult = straightFlushProcessor.processHand(hand, board);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Td, Cards.Jd, Cards.Qd, Cards.Kd, Cards.Ad);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.StraightFlush, HandType.HighHand, "HandB", expectedCards);
    }*/

    @Test
    public void testNoStraightFlush() {
        final IHandProcessor straightFlushProcessor = new StraightFlushProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.Td, Cards.N8d, Cards.N9d, Cards.Jd, Cards.Ad))
                                                    .build();
        final Either<NotQualifiedResult, QualifiedResult> handResult = straightFlushProcessor.processHand(cc);

        assertTrue(handResult.isEmpty());
    }

}
