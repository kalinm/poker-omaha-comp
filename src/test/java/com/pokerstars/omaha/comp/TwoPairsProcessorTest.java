package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TwoPairsProcessorTest {

    //Here we have 2xA + 2xQ + 2xK and expecting 2xA + 2xK
    @Test
    public void testTwoPairs2xA2xK() {
        final IHandProcessor twoPairsProcessor = new TwoPairsProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Ad, Cards.Kc, Cards.Ah, Cards.Kd, Cards.Qs))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = twoPairsProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ad, Cards.Ah, Cards.Kc, Cards.Kd, Cards.Qs);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.TwoPair, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testFullHouse2xK2xA() {
        final IHandProcessor twoPairsProcessor = new TwoPairsProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.Kd, Cards.Kc, Cards.Tc, Cards.Ah, Cards.Ad))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = twoPairsProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Kc, Cards.Kd, Cards.Ad, Cards.Ah, Cards.Tc);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.TwoPair, HandType.HighHand, "HandA", expectedCards);
    }

}
