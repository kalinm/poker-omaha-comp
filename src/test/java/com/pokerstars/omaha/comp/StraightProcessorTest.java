package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Assert;
import org.junit.Test;

import static com.pokerstars.omaha.utils.OmahaCompTestUtils.validateCombination;
import static org.junit.Assert.*;

public class StraightProcessorTest {

    @Test
    public void validRank() {
        final IHandProcessor straightProcessor = new StraightProcessor();

        Assert.assertEquals(HandRank.Straight, straightProcessor.getRank());
    }

    @Test
    public void testStraightNoAce() {
        final IHandProcessor straightProcessor = new StraightProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Qs, Cards.Jc, Cards.Th, Cards.N9s, Cards.Ks))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N9s, Cards.Th, Cards.Jc, Cards.Qs, Cards.Ks);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.Straight, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testStraightLowAce() {
        final IHandProcessor straightProcessor = new StraightProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandC")
                                                  .cards(List.of(Cards.N2c, Cards.N5s, Cards.N3c, Cards.N4h, Cards.Ac))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ac, Cards.N2c, Cards.N3c, Cards.N4h, Cards.N5s);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.Straight, HandType.HighHand, "HandC", expectedCards);
    }

    @Test
    public void testStraightHighAce() {
        final IHandProcessor straightProcessor = new StraightProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.Qs, Cards.Kc, Cards.Td, Cards.Jh, Cards.Ad))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Td, Cards.Jh, Cards.Qs, Cards.Kc, Cards.Ad);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.Straight, HandType.HighHand, "HandA", expectedCards);
    }

    // Here the ace could act both for high or low straight flush
   /* @Test
    public void testStraightProcessor2PossibleStraights() {
        final IHandProcessor straightProcessor = new StraightProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandB")
                                                    .cards(List.of(Cards.N2s, Cards.N5d, Cards.N3h, Cards.Ad))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ts, Cards.Js, Cards.Qd, Cards.Kh, Cards.Ad);

        assertEquals(5, qualifiedResult.getCards().size());

        validateCombination(qualifiedResult, HandRank.Straight, HandType.HighHand, "HandB", expectedCards);
    }*/

    @Test
    public void testNoStraight() {
        final IHandProcessor straightProcessor = new StraightProcessor();
        final CardCombination cc = CardCombination.builder()
                                                    .combinationType(CombinationType.Hand)
                                                    .id("HandA")
                                                    .cards(List.of(Cards.N2d, Cards.N8h, Cards.N9c, Cards.Td, Cards.N4c))
                                                    .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = straightProcessor.processHand(cc);

        assertTrue(handResult.isEmpty());
    }

}
