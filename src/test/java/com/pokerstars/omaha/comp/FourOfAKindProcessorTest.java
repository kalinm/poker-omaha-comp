package com.pokerstars.omaha.comp;

import com.pokerstars.omaha.data.CardCombination;
import com.pokerstars.omaha.data.Cards;
import com.pokerstars.omaha.data.CombinationType;
import com.pokerstars.omaha.data.HandRank;
import com.pokerstars.omaha.data.HandType;
import com.pokerstars.omaha.data.NotQualifiedResult;
import com.pokerstars.omaha.data.QualifiedResult;
import com.pokerstars.omaha.utils.OmahaCompTestUtils;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class FourOfAKindProcessorTest {

    @Test
    public void testValidRank() {
        final IHandProcessor fourOfAKindProcessor = new FourOfAKindProcessor();

        Assert.assertEquals(HandRank.FourOfAKind, fourOfAKindProcessor.getRank());
    }

    @Test
    public void testFindFourOfAKind() {
        final IHandProcessor fourOfAKindProcessor = new FourOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Combined)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.As, Cards.N2d, Cards.N2c, Cards.N2s, Cards.N2h))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = fourOfAKindProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.N2d, Cards.N2c, Cards.N2s, Cards.N2h, Cards.As);

        assertEquals(5, qualifiedResult.getCards().size());

        OmahaCompTestUtils.validateCombination(qualifiedResult, HandRank.FourOfAKind, HandType.HighHand, "HandA", expectedCards);
    }

    /**
     * Here we have 4xJ + 4xA and expect four aces
     */
    @Test
    public void testFourOfAKindX2() {
        final IHandProcessor fourOfAKindProcessor = new FourOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.Jd, Cards.Ah, Cards.Ad, Cards.Ac, Cards.As))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = fourOfAKindProcessor.processHand(cc);

        final QualifiedResult qualifiedResult = handResult.get();

        assertNotNull(qualifiedResult);

        final List<Cards> expectedCards = List.of(Cards.Ad, Cards.Ac, Cards.As, Cards.Ah, Cards.Jd);

        assertEquals(5, qualifiedResult.getCards().size());

        OmahaCompTestUtils.validateCombination(qualifiedResult, HandRank.FourOfAKind, HandType.HighHand, "HandA", expectedCards);
    }

    @Test
    public void testNoFindFourOfAKind() {
        final IHandProcessor fourOfAKindProcessor = new FourOfAKindProcessor();
        final CardCombination cc = CardCombination.builder()
                                                  .combinationType(CombinationType.Hand)
                                                  .id("HandA")
                                                  .cards(List.of(Cards.N3d, Cards.N2c, Cards.N9c, Cards.Ac, Cards.N5s))
                                                  .build();

        final Either<NotQualifiedResult, QualifiedResult> handResult = fourOfAKindProcessor.processHand(cc);

        assertTrue(handResult.isEmpty());
    }

}
