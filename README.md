# README #

 Omaha-comp PokerStars Task

### What is this repository for? ###

This is a codding task for evaluating the wins of Omaha poker game.
Please see the doc for more information
It uses Akka actor system to distribute the load

### How do I get set up? ###

* please checkout the repository - git clone git@bitbucket.org:kalinm/poker-omaha-comp.git
* run mvn install
* execute com.pokerstars.omaha.OmahaComp.main()

### External libraries ###
* Vavr
* Lombok (you might need the lombok plugin for Intellij or whatever your IDE is)
* Akka

### Contribution guidelines ###

* Any feedback is welcomed

### Who do I talk to? ###

* Kalin Muzdrakov - kalin.muzdrakov@gmail.com 